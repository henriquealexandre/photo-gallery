package main

import (
	"fmt"
	"github.com/calhoun/models"
)

const (
	host     = "localhost"
	port     = "5432"
	user     = "henrique"
	password = "alwaysbekind"
	dbname   = "photoG"
)

func main() {
	//Generating the connection string
	psqlInfo := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
		host, port, user, password, dbname)

	us, err := models.NewUserService(psqlInfo)
	if err != nil {
		panic(err)
	}
	//Close the db connection once it's not in use anymore
	defer us.Close()
	//us.DestructiveReset()
	//us.DestructiveReset()
	us.AutoMigrate()

	user := models.User{
		Name: "Jon Calhoun",
		Email: "jon@jon.io",
		Password: "jon",
		Token: "abc123",
	}

	us.Create(&user)


	fmt.Printf("%+v\n", user)

	user2, err := us.ByToken(("abc123"))
	if err != nil {
		panic(err)
	}

	fmt.Printf("%+v\n", user2)

}
