/*
*Middleware are pieces of code that get inserted in between the net/http package and our router,
or between our router and controllers.
Middleware is just a fancy name for inserting code in between parts applications.
*/
package middleware

import (
	"net/http"

	"github.com/calhoun/context"
	"github.com/calhoun/models"
)

type User struct {
	models.UserService
}

func (mw *User) Apply(next http.Handler) http.HandlerFunc {
	return mw.ApplyFunction(next.ServeHTTP)
}

func (mw *User) ApplyFunction(next http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		cookie, err := r.Cookie("remember_token")
		if err != nil {
			next(w, r)
			return
		}
		user, err := mw.UserService.ByToken(cookie.Value)
		if err != nil {
			next(w, r)
			return
		}
		ctx := r.Context()
		ctx = context.WithUser(ctx, user)
		r = r.WithContext(ctx)
		//fmt.Println("User found: ", user)
		//fmt.Fprintf(w, "%+v\n", user)

		//if the user is logged in
		next(w, r)
	})

}

type RequireUser struct {
	User
}

func (mw *RequireUser) Apply(next http.Handler) http.HandlerFunc {
	return mw.ApplyFunction(next.ServeHTTP)
}

func (mw *RequireUser) ApplyFunction(next http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		user := context.User(r.Context())
		if user == nil {
			http.Redirect(w, r, "/login", http.StatusFound)
			return
		}
		next(w, r)
	}) /*
			cookie, err := r.Cookie("remember_token")
			if err != nil {
				http.Redirect(w, r, "/login", http.StatusFound)
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			user, err := mw.UserService.ByToken(cookie.Value)
			if err != nil {
				http.Redirect(w, r, "/login", http.StatusFound)
				//http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			ctx := r.Context()
			ctx = context.WithUser(ctx, user)
			r = r.WithContext(ctx)
			//fmt.Println("User found: ", user)
			//fmt.Fprintf(w, "%+v\n", user)

			//if the user is logged in
			next(w, r)

		})
							--> replaced by the code above
	*/
}
