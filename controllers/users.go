package controllers

import (
	"fmt"
	"log"
	"net/http"

	"github.com/calhoun/rand"

	"github.com/calhoun/models"
	"github.com/calhoun/views"
)

//SignupForm contains fields that are present in the incoming form.
//It's used to process incoming data from the web request .
type SignupForm struct {
	/*
		Struct tags are a form of declaring metadata.
		They are not checked by the compiler,
		but are useful to identify the type of data we are working with,
		since they make easier to encode and decode data.
		The reflect package is required to use tags.
		We should use them only when it's really necessary
		(debugging might become a challenge given they are skipped by the compiler.)

		Make sure the template (new.gohtml) reflect precisely the names of the following fields (name, email and pass lowercase).
	*/

	Name     string `schema:"name"`
	Email    string `schema:"email"`
	Password string `schema:"password"`
}

/*
LoginForm contains the fields available in the Login page.
We could reuse the Signup form instead of spinning up a new one, but separate structs scale better.
*/
type LoginForm struct {
	Email    string `schema:"email"`
	Password string `schema:"password"`
}

//Users is a type that represents user main resources.
type Users struct {
	NewView   *views.View
	LoginView *views.View
	//We don't a pointer because we are not using a struct anymore, but an interface.
	us models.UserService
}

//NewUsers create/initialises a new Users controller
func NewUsers(us models.UserService) *Users {
	return &Users{
		NewView:   views.NewView("bootstrap", "users/new"),
		LoginView: views.NewView("bootstrap", "users/login"),
		us:        us,
	}
}

/*
New is used to RENDER the form WHERE a user can create a new account.
Instead of declaring global vars in the entry point (main function),
we can render the object using this method.
GET /signup
*/
func (u *Users) New(w http.ResponseWriter, r *http.Request) {
	u.NewView.Render(w, r, nil)
}

/*
Create is used to PROCESS the signup form WHEN a user submits it.
It's used to create a new user account.
POST /signup
*/
func (u *Users) Create(w http.ResponseWriter, r *http.Request) {

	var form SignupForm
	var vd views.Data

	/*
		Moved parseForm to a helper function to DRY up
		printError(parseForm(r, &form))
						--> Replaced by the following block of code to permit alert initialisation
	*/

	err := parseForm(r, &form)
	if err != nil {
		log.Println(err)
		vd.SetAlert(err)

		/*
			vd.Alert = &views.Alert{
				Level:   views.AlertLevelError,
				Message: views.AlertGeneric,
			}					--> block replaced by the line just above
		*/
		//Passing response along with the error object
		u.NewView.Render(w, r, vd)
		return
	}

	user := models.User{
		Name:     form.Name,
		Email:    form.Email,
		Password: form.Password,
	}

	if err := u.us.Create(&user); err != nil {
		vd.SetAlert(err)
		/*
			vd.Alert = &views.Alert {
				Level:   views.AlertLevelError,
				Message: err.Error(),
			}				--> replaced by the line above as well.
		*/

		u.NewView.Render(w, r, vd)
		return
	}
	//http.Error(w, err.Error(), http.StatusInternalServerError)
	//return				--> Replaced by the code just above

	err = u.signIn(w, &user)
	if err != nil {
		http.Redirect(w, r, "/login", http.StatusFound)
		return

		//Replaced by the code just above
		//http.Error(w, err.Error(), http.StatusInternalServerError)
		//return
	}
	http.Redirect(w, r, "/galleries", http.StatusFound)
}

/*
Login is used to verify the given email address and password and then log the user if they are correct.
POST /login
*/
func (u *Users) Login(w http.ResponseWriter, r *http.Request) {

	//var form LoginForm is equivalent
	form := LoginForm{}
	//vd is the object which will embed the errors
	vd := views.Data{}

	//Populating the LoginForm object.
	err := parseForm(r, &form)
	if err != nil {
		log.Println(err)
		vd.SetAlert(err)
		u.LoginView.Render(w, r, vd)
		return
	}
	//fmt.Fprintln(w, form)		--> replaced by the print statement down below.

	user, err := u.us.Authenticate(form.Email, form.Password)
	if err != nil {
		switch err {
		case models.ErrNotFound:
			/*
				fmt.Fprintln(w, "Invalid email address.")
					case models.ErrPasswordIncorrect:
				fmt.Fprintln(w, "Invalid password provided.")
							--> old response
			*/

			/*
				vd.Alert = &views.Alert{
					Level:   views.AlertLevelError,
					Message: "Invalid email address",
				}				-->replaced by the line below
			*/

			vd.AlertError("Invalid email address")

		default:
			vd.SetAlert(err)
			//http.Error(w, err.Error(), http.StatusInternalServerError)
			//We don't need to print the error in the response writer anymore, since we can pass them inside vd.
		}
		//We have the alert set already coming through the flow of the switch statement above.
		u.LoginView.Render(w, r, vd)
		return
	}

	//The user in this block is a pointer already, so no need to identify it.
	err = u.signIn(w, user)
	if err != nil {
		//http.Error(w, err.Error(), http.StatusInternalServerError)
		vd.SetAlert(err)
		u.LoginView.Render(w, r, vd)
		return
	}

	http.Redirect(w, r, "/galleries", http.StatusFound)

}

//signIn sets up a cookie in the browser so the user can be remembered. User is signed in via cookie.
func (u *Users) signIn(w http.ResponseWriter, user *models.User) error {
	//Make sure the user token is available for signing in
	if user.Token == "" {
		token, err := rand.RememberToken()
		if err != nil {
			return err
		}
		user.Token = token
		err = u.us.Update(user)
		if err != nil {
			return err
		}
	}

	cookie := http.Cookie{
		//Map structure
		Name:  "remember_token",
		Value: user.Token,
		//Preventing JS injection
		HttpOnly: true,
	}

	//Set up cookie into the response
	http.SetCookie(w, &cookie)
	return nil

}

//CookieTest displays current user info available in the cookie
func (u *Users) CookieTest(w http.ResponseWriter, r *http.Request) {
	cookie, err := r.Cookie("remember_token")
	if err != nil {
		http.Redirect(w, r, "/login", http.StatusFound)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	user, err := u.us.ByToken(cookie.Value)
	if err != nil {
		http.Redirect(w, r, "/login", http.StatusFound)
		//http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	fmt.Fprintf(w, "%+v\n", user)

}

/*
The signatures below are equivalent.
Methods are a nice way to organise captured data.
*/

// func (u *Users) New(w http.ResponseWriter, r *http.Request) {}
// func New (u *Users, w http.ResponseWriter, r *http.Request) {}

func printError(err error) {
	if err != nil {
		log.Println(err)
	}
}
