package controllers

import (
	"net/http"

	"github.com/gorilla/schema"
)

func parseForm(r *http.Request, dst interface{}) error {

	//ParseForm populates PostForm.
	printError(r.ParseForm())

	//Gorilla-Schema decoder will be used to convert data into the form struct (as strings, in this case).
	decoder := schema.NewDecoder()

	//Decode decodes a map of strings to a struct.
	//URL values are of the same type of PostForm `map[string][]string`
	if err := decoder.Decode(dst, r.PostForm); err != nil {
		return err
	}
	return nil

}
