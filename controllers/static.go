package controllers

import "github.com/calhoun/views"

//Static views represent pages that don't handle dynamic resources
type Static struct {
	HomeView    *views.View
	ContactView *views.View
}

//NewStaticView initialises an object containing parsed static views
func NewStaticView() *Static {

	return &Static{
		HomeView:    views.NewView("bootstrap", "static/home"),
		ContactView: views.NewView("bootstrap", "static/contact"),
	}
}
