package controllers

import (
	"fmt"
	"log"
	"net/http"
	"strconv"

	"github.com/calhoun/context"
	"github.com/calhoun/models"
	"github.com/calhoun/views"
	"github.com/gorilla/mux"
)

const ShowGalleryName = "show_gallery"
const EditGalleryName = "edit_gallery"
const maxMem = 1 << 30

//GalleryForm contains Title, that is one attribute of every gallery.
type GalleryForm struct {
	//galleries.gohtml must reflect title to have this working.
	Title string `schema:"title"`
}

//Galleries is a structure that represents galleries main resources.
type Galleries struct {
	New       *views.View
	IndexView *views.View
	EditView  *views.View
	ShowView  *views.View
	gs        models.GalleryService
	is        models.ImageService
	router    *mux.Router
}

//NewGalleries create/initialises a new Galleries controller
func NewGalleries(gs models.GalleryService, is models.ImageService, router *mux.Router) *Galleries {
	return &Galleries{
		New:       views.NewView("bootstrap", "galleries/new"),
		IndexView: views.NewView("bootstrap", "galleries/index"),
		EditView:  views.NewView("bootstrap", "galleries/edit"),
		ShowView:  views.NewView("bootstrap", "galleries/show"),
		gs:        gs,
		is:        is,
		router:    router,
	}
}

//POST /galleries
func (g *Galleries) Create(w http.ResponseWriter, r *http.Request) {

	var form GalleryForm
	var vd views.Data

	err := parseForm(r, &form)
	if err != nil {
		log.Println(err)
		vd.SetAlert(err)

		g.New.Render(w, r, vd)
		return
	}

	user := context.User(r.Context())
	fmt.Println(user)

	gallery := models.Gallery{
		Title:  form.Title,
		UserID: user.ID,
	}

	if err := g.gs.Create(&gallery); err != nil {
		vd.SetAlert(err)

		g.New.Render(w, r, vd)
		return
	}
	url, err := g.router.Get(EditGalleryName).URL("id", fmt.Sprintf("%v", gallery.ID))
	if err != nil {
		http.Redirect(w, r, "/", http.StatusFound)
		return
	}
	http.Redirect(w, r, url.Path, http.StatusFound)
	//fmt.Fprintln(w, gallery)
}

func (g *Galleries) Index(w http.ResponseWriter, r *http.Request) {
	user := context.User(r.Context())
	galleries, err := g.gs.ByUserID(user.ID)
	if err != nil {
		http.Error(w, "Something went wrong!", http.StatusInternalServerError)
		return
	}

	var vd views.Data
	vd.Yield = galleries
	//fmt.Fprintln(w, galleries)
	g.IndexView.Render(w, r, vd)
}

//GET /galleries/:id
func (g *Galleries) Show(w http.ResponseWriter, r *http.Request) {

	gallery, err := g.galleryByID(w, r)
	if err != nil {
		return
	}

	var vd views.Data
	vd.Yield = gallery
	g.ShowView.Render(w, r, vd)
	//fmt.Fprintln(w, gallery)
}

func (g *Galleries) Edit(w http.ResponseWriter, r *http.Request) {
	gallery, err := g.galleryByID(w, r)
	if err != nil {
		return
	}

	user := context.User(r.Context())
	if gallery.UserID != user.ID {
		http.Error(w, "Gallery not found", http.StatusNotFound)
		return
	}

	var vd views.Data
	vd.Yield = gallery
	g.EditView.Render(w, r, vd)
}

func (g *Galleries) Update(w http.ResponseWriter, r *http.Request) {
	gallery, err := g.galleryByID(w, r)
	if err != nil {
		return
	}

	user := context.User(r.Context())
	if gallery.UserID != user.ID {
		http.Error(w, "Gallery not found", http.StatusNotFound)
		return
	}

	var vd views.Data
	vd.Yield = gallery

	var form GalleryForm

	err = parseForm(r, &form)
	if err != nil {
		log.Println(err)
		vd.SetAlert(err)

		g.EditView.Render(w, r, vd)
		return
	}

	gallery.Title = form.Title
	err = g.gs.Update(gallery)
	if err != nil {
		vd.SetAlert(err)
		g.EditView.Render(w, r, vd)
		return
	}

	vd.Alert = &views.Alert{
		Level:   views.AlertLevelSuccess,
		Message: "Gallery successfully updated",
	}
	g.EditView.Render(w, r, vd)
	//fmt.Fprintln(w, gallery)
	//g.EditView.Render(w, r, vd)
}

func (g *Galleries) Delete(w http.ResponseWriter, r *http.Request) {
	gallery, err := g.galleryByID(w, r)
	if err != nil {
		return
	}

	user := context.User(r.Context())
	if gallery.UserID != user.ID {
		http.Error(w, "Gallery not found", http.StatusNotFound)
		return
	}
	var vd views.Data
	err = g.gs.Delete(gallery.ID)
	if err != nil {
		vd.SetAlert(err)
		vd.Yield = gallery
		g.EditView.Render(w, r, vd)
		return
	}

	//fmt.Fprintln(w, "successfully deleted!")
	http.Redirect(w, r, "/galleries", http.StatusFound)
}

func (g *Galleries) galleryByID(w http.ResponseWriter, r *http.Request) (*models.Gallery, error) {
	vars := mux.Vars(r)
	idString := vars["id"]
	id, err := strconv.Atoi(idString)
	if err != nil {
		http.Error(w, "Invalid Gallery ID", http.StatusNotFound)
		return nil, err
	}
	gallery, err := g.gs.ByID(uint(id))
	if err != nil {
		switch err {
		case models.ErrNotFound:
			http.Error(w, "Gallery not found", http.StatusNotFound)
			//return
		default:
			http.Error(w, "Something went wrong", http.StatusInternalServerError)
			//return
		}
		return nil, err
	}

	return gallery, nil
}

func (g *Galleries) ImageUpload(w http.ResponseWriter, r *http.Request) {
	gallery, err := g.galleryByID(w, r)
	if err != nil {
		return
	}

	user := context.User(r.Context())
	if gallery.UserID != user.ID {
		http.Error(w, "Gallery not found", http.StatusNotFound)
		return
	}

	var vd views.Data
	err = r.ParseMultipartForm(maxMem)
	if err != nil {
		vd.SetAlert(err)
		g.EditView.Render(w, r, vd)
		return
	}
	/*
		galleryPath := fmt.Sprintf("images/galleries/%v/", gallery.ID)
		err = os.MkdirAll(galleryPath, 0755)
		if err != nil {
			vd.SetAlert(err)
			g.EditView.Render(w, r, vd)
			return
		}
								--> replaced by is.makeImagePath
	*/

	//images is the name of the file field declared in the editForm>uploadImageForm
	files := r.MultipartForm.File["images"]

	for _, f := range files {
		file, err := f.Open()
		if err != nil {
			vd.SetAlert(err)
			g.EditView.Render(w, r, vd)
			return
		}
		defer file.Close()

		err = g.is.Create(gallery.ID, file, f.Filename)
		if err != nil {
			vd.SetAlert(err)
			g.EditView.Render(w, r, vd)
			return
		}
		fmt.Fprintln(w, "Files successfully uploaded")
	}
}
