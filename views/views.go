package views

import (
	"bytes"
	"html/template"
	"io"
	"log"
	"net/http"
	"path/filepath"

	"github.com/calhoun/context"
)

var (
	layoutFolder      string = "views/layouts/"
	templateDir       string = "views/"
	templateExtension string = ".gohtml"
)

type View struct {
	Template *template.Template
	Layout   string
}

//The ServeHTTP interface replaces the necessity of declaring a new function to render each page
func (v *View) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	v.Render(w, r, nil)
}

/*
Render method renders the view with a pre-defined layout.
We could use io.writer instead of an empty interface,
but the more generic the better to work with http.Requests down the line.
*/
func (v *View) Render(w http.ResponseWriter, r *http.Request, data interface{}) {

	//defining response metadata, the format of returned data.
	w.Header().Set("Content-Type", "text/html")

	var vd Data

	//We make a type switch here to avoid unexpected errors when a type different from Data is passed over as data.
	switch d := data.(type) {
	case Data:
		vd = d
		//do nothing, since the Data is valid to render the bootstrap page alert.
	default:
		//case not, yield the string (or any other value) to be embedded in the correspondent data type.
		vd = Data{
			Yield: data,
		}
	}
	/*
		The layout is the one parsed in the NewView function.
		Although we have defined two parameters in the method signature,
		the ExecuteTemplate function requires three, one being defined previously.
	*/
	vd.User = context.User(r.Context())
	var buffer bytes.Buffer
	

	err := v.Template.ExecuteTemplate(&buffer, v.Layout, data)
	if err != nil {
		http.Error(w, "Something went wrong.", http.StatusInternalServerError)
		return
	}
	io.Copy(w, &buffer)

}

/*
NewView creates a view (struct) and parses templates into it.
Using variadic parameters we eliminate the necessity of iteration over a list.
fileNames refer to the name of primary views (like home, contact, etc).
Variadic parameters MUST be the last ones in the params list.
*/
func NewView(layout string, fileNames ...string) *View {

	//fmt.Println("Before: ", fileNames)
	addTemplatePath(fileNames)
	//fmt.Println("After Path: ", fileNames)
	addTemplateExtension(fileNames)
	//fmt.Println("After Extension: ", fileNames)

	//function append takes a variadic parameter as the last argument
	//we use the '...' notation to unpack/unfurl the slice of strings.
	fileNames = append(fileNames, layoutFiles()...)

	//Returns a parsed template type
	t, err := template.ParseFiles(fileNames...)
	printError(err)

	//Initialises a pointer to a View embedding the parsed template.
	return &View{
		Template: t,
		Layout:   layout,
	}
}

//layoutFiles returns the names of the layout files in a slice of strings.
func layoutFiles() []string {
	files, err := filepath.Glob(layoutFolder + "*" + templateExtension)
	printError(err)

	return files
}

/*
addTemplatePath takes in a slice of strings representing file paths for templates
and prepends the templateDir directory to each string in the slice.
Eg.: `home` will become `views/home`, given the template dir is set to `views/`
*/
func addTemplatePath(files []string) {
	for i, j := range files {
		files[i] = templateDir + j
	}

}

/*
addTemplateExtension takes in a slice of strings representing file paths for templates
and appends the templateExtension to each string in the slice.
Eg.: `home` will become `home.gohtml`, given the templateExtension is set to `.gohtml`
*/
func addTemplateExtension(files []string) {
	for i, j := range files {
		files[i] = j + templateExtension
	}

}

func printError(err error) {
	if err != nil {
		log.Println(err)
	}
}
