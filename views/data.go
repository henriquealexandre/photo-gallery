package views

import "github.com/calhoun/models"

//Constants representing Bootstrap alert levels
const (
	//AlertGeneric is displayed when any random error is encountered.
	AlertGeneric      = "Sorry, something went wrong."
	AlertLevelError   = "danger"
	AlertLevelWarning = "warning"
	AlertLevelInfo    = "info"
	AlertLevelSuccess = "success"
)

//Alert allows us to define dynamic alert content, rendering Bootstrap alerts messages into the template. (Check bootstrap and alert.gohtml)
type Alert struct {
	Level   string
	Message string
}

//Data is the top level structure that views expect data to come in.
type Data struct {
	/*
		We make this as a pointer because bootstrap is receiving data from the following object
		and sometimes the alert might be nil.
	*/
	Alert *Alert
	User  *models.User
	Yield interface{}
}

/*
SetAlert handles which error message will be displayed for the end user.
We use a pointer in the receiver to allow data object modification through invoking its method.
*/
func (d *Data) SetAlert(e error) {
	//Assertion. If 'e' implements the interface, then proceed --ok is true--, else...
	if pubErr, ok := e.(PublicError); ok {
		d.Alert = &Alert{
			Level:   AlertLevelError,
			Message: pubErr.Public(),
		}
	} else {
		d.Alert = &Alert{
			Level:   AlertLevelError,
			Message: AlertGeneric,
		}
	}
}

func (d *Data) AlertError(msg string) {
	d.Alert = &Alert{
		Level:   AlertLevelError,
		Message: msg,
	}
}

type PublicError interface {
	error
	Public() string
}
