package models

import "github.com/jinzhu/gorm"

//firstRecord will query using the provided gorm.DB and place the first returned item into dst.
func firstRecord(db *gorm.DB, dst interface{}) error {

	err := db.First(dst).Error
	if err == gorm.ErrRecordNotFound {
		return ErrNotFound
	}
	return err
}

//func (us *UserService) DestructiveReset() error	--> Method type changed from us to ug
/*
//Close closes the UserService database connection
func (ug *userGorm) Close() error {
	return ug.db.Close()
}

//DestructiveReset drops a table if it exists.
func (ug *userGorm) DestructiveReset() error {
	if err := ug.db.DropTableIfExists(&User{}).Error; err != nil {
		return err
	}
	return nil
}

//func (us *UserService) AutoMigrate() error	--> Method type changed from us to ug

//AutoMigrate builds a table according to the GORM object.
func (ug *userGorm) AutoMigrate() error {
	if err := ug.db.AutoMigrate(&User{}).Error; err != nil {
		return err
	}
	return nil
}
*/