package models

import (
	"github.com/calhoun/hash"
	"golang.org/x/crypto/bcrypt"
	"regexp"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

const userPasswordPepper = "random-string"
const hmacKey = "secret-key"

//User represents a SQL user object embedding a GORM model.
type User struct {
	gorm.Model
	Name  string
	Email string `gorm:"not null;unique_index"`
	//Passwords MUST NOT be stored in the DB. We inform GORM that through the following tag.
	Password     string `gorm:"-"`
	PasswordHash string `gorm:"not null"`
	//User will be remembered by this token. It functions like a second password
	Token     string `gorm:"-"`
	//TokenHash stores an encrypted version of our password and salt. ;)
	TokenHash string `gorm:"not null;unique_index"`
}

type userGorm struct {
	db   *gorm.DB
}

type userService struct {
	UserDB
}

type UserValidator struct {
	UserDB
	hmac hash.HMAC
	emailRegex *regexp.Regexp
}

//UserDB is used to interact with database users.
type UserDB interface {
	//Methods for querying for single users
	ByID(id uint) (*User, error)
	ByEmail(email string) (*User, error)
	ByToken(token string) (*User, error)

	//Methods for altering users
	Create(user *User) error
	Update(user *User) error
	Delete(id uint) error
	/*
	//Used to close a DB connection
	Close() error

	//Migration helpers
	DestructiveReset() error
	AutoMigrate() error
	*/

}

//UserService contains a set of methods used to manipulate and work with user models.
type UserService interface {
	Authenticate(email, password string) (*User, error)
	UserDB
}

//Testing interface implementations
var _ UserDB = &userGorm{}
var _ UserDB = &UserValidator{}
var _ UserService = &userService{}



func NewUserService(db *gorm.DB) UserService {
	ug := &userGorm{db}
	hmac := hash.NewHMAC(hmacKey)
	uv := newUserValidator(ug, hmac)
	return &userService{
		UserDB: uv,
	}

}

/*				--> Replaced by the function above
func NewUserService(connectionString string) (UserService, error) {
	ug, err := newUserGorm(connectionString)
	if err != nil {
		return nil, err
	}

	hmac := hash.NewHMAC(hmacKey)
	uv := newUserValidator(ug, hmac)
	return &userService{
		UserDB: uv,
	}, nil

}


func newUserGorm(connectionString string) (*userGorm, error) {
	db, err := gorm.Open("postgres", connectionString)
	if err != nil {
		return nil, err
	}

	db.LogMode(true)

	//hmac := hash.NewHMAC(hmacKey)		--> moving to UserValidator. We could keep it in both but it is not needed.
	return &userGorm{
		db:   db,
		//hmac: hmac,
	}, nil
}
				--> Replaced by NewUserService v2 (just above).
*/

/*
Authenticate authenticates a user by his given credential.
If the user and password are correct, the correspondent user will be returned.
*/
func (us *userService) Authenticate(email, password string) (*User, error) {
	//Look up the user
	foundUser, err := us.ByEmail(email)
	if err != nil {
		return nil, err
	}

	//Verify if the user password is valid.
	err = bcrypt.CompareHashAndPassword(
		[]byte(foundUser.PasswordHash),
		[]byte(password+userPasswordPepper))
	if err != nil {
		switch err {
		case bcrypt.ErrMismatchedHashAndPassword:
			return nil, ErrPasswordIncorrect
		default:
			return nil, err
		}
	}

	return foundUser, nil
}

