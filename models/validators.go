package models

//ByToken will hash the given token and call ByToken on the subsequent DB layer.
func (uv *UserValidator) ByToken(token string) (*User, error) {

	user := User {
		Token: token,
	}
	err := userValidationFun(&user, uv.hmacToken);
	if err != nil {
		return nil, err
	}
	//tokenHash := uv.hmac.Hash(token)		--> Replaced by the valFun hmacToken used just above.
	return uv.UserDB.ByToken(user.TokenHash)
}

//ByEmail normalises the email address before calling the respective UserDB method.
func (uv *UserValidator) ByEmail(email string) (*User, error) {
	user := User {
		Email: email,
	}
	err := userValidationFun(&user, uv.normaliseEmail);
	if err != nil {
		return nil, err
	}

	//Calling function to lookup user by email.
	return uv.UserDB.ByEmail(user.Email)
}

//Create hashes the user password, erases the provided password in the call and invoke GORM create method.
func (uv *UserValidator) Create(user *User) error {

	err := userValidationFun(user,
		uv.passwordRequired,
		uv.passwordMinLength,
		uv.bcryptPassword,
		uv.passwordHashRequired,
		uv.setToken,
		uv.tokenMinBytes,
		uv.hmacToken,
		uv.tokenHashRequired,
		uv.normaliseEmail,
		uv.requireEmail,
		uv.emailFormat,
		uv.emailIsAvailable)
	if err != nil {
		return err
	}

	//user.TokenHash = uv.hmac.Hash(user.Token)		--> Replaced by the valFun hmacToken used above.

	//Inserting user into the database.
	return uv.UserDB.Create(user)
}

//Update will hash a token if it is provided.
func (uv *UserValidator) Update(user *User) error{
	err := userValidationFun(user,
		uv.passwordMinLength,
		uv.bcryptPassword,
		uv.passwordHashRequired,
		uv.tokenMinBytes,
		uv.hmacToken,
		uv.tokenHashRequired,
		uv.normaliseEmail,
		uv.requireEmail,
		uv.emailFormat,
		uv.emailIsAvailable)
	if err != nil {
		return err
	}

	return uv.UserDB.Update(user)
}

//Delete verifies if the user id provided is valid or not. If it is, GORM delete method is called.
func (uv *UserValidator) Delete(id uint) error {
	var user User
	user.ID = id
	err := userValidationFun(&user, uv.idGtZero)
	if err != nil {
		return err
	}
	return uv.UserDB.Delete(id)
}