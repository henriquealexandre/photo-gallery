package models

import "strings"

const (
	//Returned when a resource is not found in the Database.
	ErrNotFound modelError = "models: resource not found"

	//Returned when an invalid password is used when attempting to authenticate a user.
	ErrPasswordIncorrect modelError = "models: incorrect password provided"

	//Returned when create is attempted without a user password provided.
	ErrPasswordRequired modelError = "models: password required"

	//Returned by create and updated methods when the password provided is less than eight characters.
	ErrPasswordTooShort modelError = "models: password must be at least eight characters long"

	//Returned when an email address is not provided when creating a user.
	ErrEmailRequired modelError = "models: email address is required"

	//Returned when an email address provided does not match the email Regex requirement
	ErrEmailInvalid modelError = "models: email address is not valid"

	//Returned when an update or create method is attempted with an address which is already in use.
	ErrEmailTaken modelError = "models: email address is already taken"

	ErrTitleRequired modelError = "models: Title is required"

	//Returned when an invalid ID is passed as a method argument.
	ErrIdInvalid privateError = "models: ID provided is invalid"

	//Returned when a token is not 32 bytes long.
	ErrTokenTooShort privateError = "models: token must be at least 32 bytes"

	//Returned when create or update is attempted without a valid token hash provided.
	ErrTokenRequired privateError = "models: token is required"

	//Returned
	ErrUserIDRequired privateError = "models: user ID is required"
)

type modelError string

func (e modelError) Error() string {
	return string(e)
}
func (e modelError) Public() string {
	//Removes the prefix of the error messages
	s := strings.Replace(string(e), "models: ", "", 1)
	//Capitalises the first letter of the phrase
	return strings.Title(s)
}

type privateError string

func (e privateError) Error() string {
	return string(e)
}