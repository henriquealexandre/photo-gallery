package models

//func (us *UserService) ByID(id uint) (*User, error)	--> Method type changed from us to ug

/*
ByID will look up the user ID that has been given and return some data if the ID matches up.
As a general rule, any error but ErrNotFound should result in a 5xx error.
*/
func (ug *userGorm) ByID(id uint) (*User, error) {
	//user will be returned if we find a user
	var user User

	//This line will be replaced by the following one to make an auxiliary function viable:
	//err := us.db.Where("id = ?", id).First(&user).Error

	db := ug.db.Where("id = ?", id)
	//If the destination is not passed as a pointer, the code will panic.
	err := firstRecord(db, &user)
	return &user, err

}

//func (us *UserService) ByEmail(email string) (*User, error)	--> Method type changed from us to ug

//ByEmail looks up a user with the given email address and returns that user. ByID and ByEmail have the same structure.
func (ug *userGorm) ByEmail(email string) (*User, error) {
	var user User

	db := ug.db.Where("email = ?", email)
	err := firstRecord(db, &user)
	return &user, err

}

//func (us *UserService) ByToken(token string) (*User, error)	--> Method type changed from us to ug

//ByToken looks up a user with the given token and returns that user. It expected to receive a token that is already hashed.
func (ug *userGorm) ByToken(tokenHash string) (*User, error) {
	var user User

	//tokenHash := ug.hmac.Hash(token)		--> moving this function to its UserValidator.

	//Instead of generating a db object before calling the function, we can sinthetise both steps in one statement.
	//We provide the token calling the function but it is never stored in the Database
	//GORM uses the snake of all fields when creating the columns in the database tables.

	err := firstRecord(ug.db.Where("token_hash = ?", tokenHash), &user)
	if err != nil {
		return nil, err
	}
	return &user, nil
}
