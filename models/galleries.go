package models

import (
	"github.com/jinzhu/gorm"
)

//Gallery is an image container
type Gallery struct {
	gorm.Model
	UserID uint   `gorm:"not_null;index"`
	Title  string `gorm:"not_null"`
}

type galleryGorm struct {
	db *gorm.DB
}

//GalleryService is used to interact with the incoming data
type GalleryService interface {
	GalleryDB
}

type GalleryDB interface {
	Create(gallery *Gallery) error
	Update(gallery *Gallery) error
	Delete(id uint) error
	ByID(id uint) (*Gallery, error)
	ByUserID(id uint) ([]Gallery, error)
}

type galleryService struct {
	GalleryDB
}

type galleryValidator struct {
	GalleryDB
}

var _ GalleryDB = &galleryGorm{}

type galleryValidatorFun func(*Gallery) error

func galleryValidationFun(gallery *Gallery, fs ...galleryValidatorFun) error {
	for _, f := range fs {
		if err := f(gallery); err != nil {
			return err
		}
	}
	return nil
}

func (gv *galleryValidator) Create(gallery *Gallery) error {
	err := galleryValidationFun(gallery,
		gv.userIDRequired,
		gv.titleRequired)
	if err != nil {
		return err
	}

	return gv.GalleryDB.Create(gallery)
}

func (gv *galleryValidator) Update(gallery *Gallery) error {
	err := galleryValidationFun(gallery,
		gv.userIDRequired,
		gv.titleRequired)
	if err != nil {
		return err
	}

	return gv.GalleryDB.Update(gallery)
}

func (gv *galleryValidator) Delete(id uint) error {
	var gallery Gallery
	gallery.ID = id
	if id <= 0 {
		return ErrIdInvalid
	}

	return gv.GalleryDB.Delete(id)
}

func (gv *galleryValidator) userIDRequired(gallery *Gallery) error {
	if gallery.UserID <= 0 {
		return ErrUserIDRequired
	}
	return nil
}

func (gv *galleryValidator) titleRequired(gallery *Gallery) error {
	if gallery.Title == "" {
		return ErrTitleRequired
	}
	return nil
}

func (gg *galleryGorm) Create(gallery *Gallery) error {
	return gg.db.Create(gallery).Error
}

func (gg *galleryGorm) Update(gallery *Gallery) error {
	return gg.db.Save(gallery).Error
}

func (gg *galleryGorm) Delete(id uint) error {
	gallery := Gallery{Model: gorm.Model{ID: id}}

	return gg.db.Delete(&gallery).Error
}

func (gg *galleryGorm) ByID(id uint) (*Gallery, error) {
	var gallery Gallery

	db := gg.db.Where("id = ?", id)

	err := firstRecord(db, &gallery)

	return &gallery, err

}

func (gg *galleryGorm) ByUserID(id uint) ([]Gallery, error) {
	var galleries []Gallery
	gg.db.Where("user_id = ?", id).Find(&galleries)
	return galleries, nil
}

func NewGalleryService(db *gorm.DB) GalleryService {
	return &galleryService{&galleryValidator{&galleryGorm{db}}}
}
