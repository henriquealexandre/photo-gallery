package models

import (
	"github.com/jinzhu/gorm"
)

//func (us *UserService) Create(user *User) error	--> Method type changed from us to ug

//Create inserts new user to the Database.
func (ug *userGorm) Create(user *User) error {
	//Inserting user into the database.
	return ug.db.Create(user).Error
}

//func (us *UserService) Update(user *User) error	--> Method type changed from us to ug

//Update will update the provided user with all the data provided in the call.
func (ug *userGorm) Update(user *User) error {
	return ug.db.Save(user).Error
}

//func (us *UserService) Delete(id uint) error	--> Method type changed from us to ug

//Delete disables a user in the DB defined by its correspondent ID
func (ug *userGorm) Delete(id uint) error {
	//Initialises an empty user with the ID received as an argument
	user := User{Model: gorm.Model{ID: id}}

	return ug.db.Delete(&user).Error
}
