package models

import (
	"github.com/calhoun/hash"
	"github.com/calhoun/rand"
	"golang.org/x/crypto/bcrypt"
	"regexp"
	"strings"
)

//Function type created to allow userValidationFun
type validatorFun func(*User) error

/*
userValidationFun takes an user and an unlimited number of validator functions
and call those functions -- referencing the user provided -- to validate user fields like password, id, name. etc.
This function was created to avoid code repetition.
*/
func userValidationFun (user *User, fs ...validatorFun) error {
	for _, f := range fs {
		if err := f(user); err != nil {
			return err
		}
	}
	return nil
}


//bcryptPassword hashes a user pass with a pepper if the password is not an empty string.
func (uv *UserValidator) bcryptPassword(user *User) error {
	if user.Password == "" {
		return nil
	}
	//Mixing password and pepper and converting the result to a byte slice.
	passwordBytes := []byte(user.Password + userPasswordPepper)

	//Bcrypt takes the password provided -- as a slice of bytes -- and returns the correspondent hash (which is an encrypted byte slice). Cost can be defined as an integer as well.
	hashedByes, err := bcrypt.GenerateFromPassword(passwordBytes, bcrypt.DefaultCost)
	if err != nil {
		return err
	}
	//Convert the hash to string and stores in the user object.
	user.PasswordHash = string(hashedByes)

	//Erasing the user's password as a safeguard.
	user.Password = ""

	return nil
}

func (uv *UserValidator) passwordRequired(user *User) error {
	if user.Password == "" {return ErrPasswordRequired}
	return nil
}

func (uv *UserValidator) passwordMinLength(user *User) error {
	if user.Password == "" {return nil}
	if len(user.Password) < 8 {return ErrPasswordTooShort}
		return nil
}

func (uv *UserValidator) passwordHashRequired(user *User) error {
	if user.PasswordHash == "" {
		return ErrPasswordRequired
	}
	return nil
}

//hmacToken verifies if the user token exists and calls the next layer method to hash it.
func (uv *UserValidator) hmacToken(user *User) error {
	if user.Token == "" {
		return nil
	}

	user.TokenHash = uv.hmac.Hash(user.Token)
	return nil

}

//setToken is just called in the Create method to set a token in the user object
func (uv *UserValidator) setToken(user *User) error{
	if user.Token != "" {
		return nil}

	token, err := rand.RememberToken()
	if err != nil {
		return err
	}
	user.Token = token
	return nil
}

func (uv *UserValidator) tokenMinBytes(user *User) error{
	if user.Token == "" {
		return nil
	}
	n, err := rand.NumberOfBytes(user.Token)
	if err != nil {
		return err
	}

	if n < 32 {
		return ErrTokenTooShort
	}
	return nil

}

func (uv *UserValidator) tokenHashRequired(user *User) error {
	if user.TokenHash == "" {
		return ErrTokenRequired
	}
	return nil
}

//idGtZero checks if user.ID is valid or not (>0).
func (uv *UserValidator) idGtZero(user *User) error{
	if user.ID <= 0 {
		return ErrIdInvalid
	}
	return nil
}

//Normalises emails converting all the characters to lowercase and removing spaces.
func (uv *UserValidator) normaliseEmail(user *User) error{
	user.Email = strings.ToLower(user.Email)
	user.Email = strings.TrimSpace(user.Email)
	return nil
}

func (uv *UserValidator) requireEmail(user *User) error{
	if user.Email == "" {
		return ErrEmailRequired
	}
	return nil
}

func (uv *UserValidator) emailFormat(user *User) error {
	if user.Email == "" {
		return nil
	}
	if !uv.emailRegex.MatchString(user.Email) {
		return ErrEmailInvalid
	}
	return nil
}

func (uv *UserValidator) emailIsAvailable(user *User) error{
	existingUser, err := uv.ByEmail(user.Email)
		if err == ErrNotFound {
			//Email address is not taken
			return nil
		}
		if err != nil {
			return err
		}
	//When we find a user by email that has an existing ID, it should be an update
	if user.ID != existingUser.ID {
		return ErrEmailTaken
	}
	return nil

}

func newUserValidator(udb UserDB, hmac hash.HMAC) *UserValidator {
	return &UserValidator{
		UserDB:     udb,
		hmac:       hmac,
		//Regular expression that matches email addresses.
		emailRegex: regexp.MustCompile(`^[a-z0-9._%+\-]+@[a-z0-9.\-]+\.[a-z]{2,16}$`),
	}
}