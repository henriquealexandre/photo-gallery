package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/calhoun/middleware"

	"github.com/calhoun/controllers"
	"github.com/calhoun/models"
	"github.com/gorilla/mux"
)

const (
	host     = "localhost"
	port     = "5432"
	user     = "henrique"
	password = "alwaysbekind"
	dbname   = "photoG"
)

//Template should be parsed once and executed everytime a user requests access to the page
func main() {

	//Generating the connection string
	psqlInfo := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
		host, port, user, password, dbname)

	services, err := models.NewServices(psqlInfo)
	printError(err)
	//us, err := models.NewUserService(psqlInfo) 		--> replaced by the code above

	//Close the db connection once it's not in use anymore
	defer services.Close()

	//services.DestructiveReset()
	services.AutoMigrate()

	//Gorilla Router/ServeMux
	router := mux.NewRouter()

	//Initialising static views. This line replaces the previous global variables.
	staticController := controllers.NewStaticView()

	//Initialising dynamic views that support [and require] DB connection.
	userController := controllers.NewUsers(services.User)

	//Initialising Galleries view.
	galleriesController := controllers.NewGalleries(services.Gallery, services.Image, router)

	//Initialising Galleries middleware
	userMiddleware := middleware.User{UserService: services.User}

	requireUser := middleware.RequireUser{
		User: userMiddleware,
	}

	//Anything that implements the interface MUST use handle, whereas anything that uses a func MUST use handleFunc.
	router.Handle("/", staticController.HomeView).Methods("GET")
	router.Handle("/contact", staticController.ContactView).Methods("GET")

	//It's ideal to split the signup and login forms in two parts: GET and POST
	router.HandleFunc("/signup", userController.New).Methods("GET")
	router.HandleFunc("/signup", userController.Create).Methods("POST")

	router.Handle("/login", userController.LoginView).Methods("GET")
	router.HandleFunc("/login", userController.Login).Methods("POST")

	router.Handle("/galleries", requireUser.ApplyFunction(galleriesController.Index)).Methods("GET")

	//galleryNew := requireUser.Apply(galleriesController.New)
	router.Handle("/galleries/new",
		requireUser.Apply(galleriesController.New)).Methods("GET")

	router.HandleFunc("/galleries",
		requireUser.ApplyFunction(galleriesController.Create)).Methods("POST")

	router.HandleFunc("/galleries/{id:[0-9]+}/edit",
		requireUser.ApplyFunction(galleriesController.Edit)).Methods("GET").Name(controllers.EditGalleryName)

	router.HandleFunc("/galleries/{id:[0-9]+}/update",
		requireUser.ApplyFunction(galleriesController.Update)).Methods("POST")

	router.HandleFunc("/galleries/{id:[0-9]+}/delete",
		requireUser.ApplyFunction(galleriesController.Delete)).Methods("POST")

	router.HandleFunc("/galleries/{id:[0-9]+}",
		galleriesController.Show).Methods("GET").Name(controllers.ShowGalleryName)
	//router.Handle("/galleries/new", galleriesController.New).Methods("GET")
	//router.HandleFunc("/galleries", galleriesController.Create).Methods("POST")

	router.HandleFunc("/galleries/{id:[0-9]+}/images",
		requireUser.ApplyFunction(galleriesController.ImageUpload)).Methods("POST")

	router.HandleFunc("/cookietest", userController.CookieTest).Methods("GET")

	//FAQ basic pilot
	router.HandleFunc("/faq", faq)

	//Customising 404 page
	router.NotFoundHandler = http.HandlerFunc(notFound)

	log.Fatal(http.ListenAndServe(":3000", userMiddleware.Apply(router)))

}

func faq(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html")

	fmt.Fprint(w, "<h1>Frequently Asked Questions</h1>")

}

func notFound(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html")

	w.WriteHeader(http.StatusNotFound)

	fmt.Fprint(w, "<h1>Sorry, page not found</h1>")
}

//created to avoid code repetition
func printError(err error) {
	if err != nil {
		log.Println(err)
	}
}
