package hash

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"hash"
)

//HMAC is a wrapper around crypto/hmac package.
type HMAC struct {
	hmac hash.Hash
}

//NewHMAC creates and returns a new HMAC object.
func NewHMAC(key string) HMAC {
	h := hmac.New(sha256.New, []byte(key))
	return HMAC {
		hmac: h,
	}
}

//Hash hashes the input using HMAC with the key provided when the object was created.
func (h HMAC) Hash(input string) string {
	h.hmac.Reset()
	h.hmac.Write([]byte(input))
	b := h.hmac.Sum(nil)
	//URLEncoding sanitises the String to be URL safe. Not needed atm, but used as good practice.
	return base64.URLEncoding.EncodeToString(b)
}