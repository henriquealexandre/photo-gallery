package main

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"fmt"
	"github.com/calhoun/hash"
)

func main() {
	/*
	//Generating random strings
	fmt.Println(rand.BuildString(10))
	fmt.Println(rand.RememberToken())
	 */

	toHash := []byte("this is my string")
	h := hmac.New(sha256.New, []byte("my-key"))
	h.Write(toHash)
	b := h.Sum(nil)
	fmt.Println(base64.URLEncoding.EncodeToString(b))

	hmac := hash.NewHMAC("my-key")
	fmt.Println(hmac.Hash("this is my string"))

	//both outputs are identical
}
