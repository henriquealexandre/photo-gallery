package main

import (
	"fmt"

	"github.com/calhoun/models"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

const (
	host     = "localhost"
	port     = "5432"
	user     = "henrique"
	password = "alwaysbekind"
	dbname   = "photoG"
)

func main() {
	//Generating the connection string
	psqlInfo := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
		host, port, user, password, dbname)

	us, err := models.NewUserService(psqlInfo)
	if err != nil {
		panic(err)
	}
	defer us.Close()

	us.DestructiveReset()

	user := models.User{
		Name:  "Marcio Sidney",
		Email: "marcio@hotmail.com",
	}

	if err := us.Create(&user); err != nil {
		panic(err)
	}

	user.Email = "marcio@uol.com.br"
	if err := us.Update(&user); err != nil {
		panic(err)
	}

	//We can call user.ID after running the update function that records the returned db results in our user.
	userTest, err := us.ByID(user.ID)
	if err != nil {
		panic(err)
	}

	fmt.Println(userTest)

	userEmailTest, err := us.ByEmail("marcio@uol.com.br")
	if err != nil {
		panic(err)
	}

	fmt.Println(userEmailTest)

	if err := us.Delete(user.ID); err != nil {
		panic(err)
	}

}
