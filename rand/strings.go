package rand

import (
	"crypto/rand"
	"encoding/base64"
)

//32 bytes = 256ˆ32 possible strings. For further reference: https://bit.ly/35P9Uwp
const RememberTokenBytes = 32

/*
BuildBytes is a helper function that generates random bytes or returns an error if there was one.
It is useful to generate tokens.
*/
func BuildBytes(n int) ([]byte, error) {
	bytes := make([]byte, n)

	//rand.Read fills the given byteSlice with random bytes.
	_, err := rand.Read(bytes)
	if err != nil {
		return nil, err
	}
	return bytes, nil

}

/*
BuildString is a helper function that generates a slice of bytes of size passed as argument
and returns a base64 URL encoded string.
It is useful to generate tokens.
*/
func BuildString(numberOfBytes int) (string, error) {
	bytes, err := BuildBytes(numberOfBytes)
	if err != nil {
		return "", err
	}
	return base64.URLEncoding.EncodeToString(bytes), nil
}

//NumberOfBytes returns the number of bytes used in the base64 URL encoded string
func NumberOfBytes (base64String string) (int, error) {
	b, err := base64.URLEncoding.DecodeString(base64String)
	if err != nil {
		return -1, err
	}
	return len(b), nil
}

//RememberToken is a helper function designed to generate tokens of a pre-determined byte size.
func RememberToken() (string, error) {
	return BuildString(RememberTokenBytes)
}
