package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

const (
	host     = "localhost"
	port     = "5432"
	user     = "henrique"
	password = "alwaysbekind"
	dbname   = "photoG"
)

//User represents a two fields SQL object  embbeding a GORM model object as well.
type User struct {
	/*
		Embbeding is not inheritance! Although they are similar,
		we might take some advantages of Go's approach, since the type carrying the model
		will receive its properties.
		Embedding provides automatic delegation.
		For further info: https://nathany.com/good/
	*/
	gorm.Model
	Name string
	//GORM way to declare NOT NULL and PK
	Email string `gorm:"not null;unique_index"`
}

//Querying documentation: https://gorm.io/docs/query.html

func main() {
	//Generating the connection string
	psqlInfo := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
		host, port, user, password, dbname)

	db, err := gorm.Open("postgres", psqlInfo)
	if err != nil {
		panic(err)
	}
	defer db.Close()

	//Testing connectivity
	if err = db.DB().Ping(); err != nil {
		panic(err)
	}
	//LogMode displays SQL statement logs in the console
	db.LogMode(true)

	//Drops table. Never user this in production!!!
	db.DropTableIfExists(&User{})

	/*
		Automigrate builds the data structure in the Database.
		This code will create a table named users
		because it is the snake case plural of User (struct type declared just above).
		The User struct is our model
	*/
	db.AutoMigrate(&User{})

	name, email := getInfo()

	u := User{
		Name:  name,
		Email: email,
	}
	//Insert user and checking for errors
	if err = db.Create(&u).Error; err != nil {
		panic(err)
	}

	var uTest User

	//Querying an specific user using PK.
	db.First(&uTest, 2)
	//Question mark acts as a placeholder. In this case it acts like a WHERE clause.
	db.First(&uTest, "age = ?", 30)

	//Customising query. We can split lines to improve readability.
	db.Where("name = ?", "Henrique").
		Where("id > ?", 2).
		First(&uTest)

	fmt.Println(uTest)

	//Querying multiple users
	var users []User
	returnedDB := db.Find(&users)
	//Error handling MUST use the returned, rather than the one used to query.
	if returnedDB.Error != nil {
		panic(err)
	}
	fmt.Println(len(users))
	fmt.Println(users)

}

//Function to prompt user to enter name and email in the console
func getInfo() (name, email string) {

	reader := bufio.NewReader(os.Stdin)
	fmt.Println("What is your name?")
	name, _ = reader.ReadString('\n')
	fmt.Println("What is your email address?")
	email, _ = reader.ReadString('\n')

	name = strings.TrimSpace(name)
	email = strings.TrimSpace(email)

	//Although it's good practice, we don't need to specify the returns.
	return

}
